program main
  
  use MODULE_CHPS_M1
  implicit none
  integer,parameter :: n=10
  real(kind=8),dimension(0:N)::X,U,D2U,du
  real(kind=8)::L,dx,tc
  real(kind=8):: err1,err2
  real(kind=8):: pi = acos(-1.)
  integer :: i
  
  L = 1
  dx = L/N
  call init_mesh(x,n,dx)

  u=0
  du=0
  d2u=0

  !> derivée seconde
  call set_variable(x,u,n,MyFunc)
  call compute_dx2(x,U,D2U,N)
  err2 = compare(x,d2u,n,D2MyFunc)

  !> derivée premiere
  call set_variable(x,u,n,MyFunc)
  call compute_dx_centre(x,U,DU,N)
  
  err1 = compare(x,du,n,DMyFunc)
  
  print*,n,err1,err2
  

contains
  
  real(kind=8) function MyFunc(x)
    implicit none
    real(kind=8) :: x
    MyFunc = x**2
  end function MyFunc
  
  real(kind=8) function dMyFunc(x)
    implicit none
    real(kind=8) :: x
    DMyFunc = 2*x
  end function dMyFunc
  
  real(kind=8) function d2MyFunc(x)
    implicit none
    real(kind=8) :: x
    D2MyFunc = 2
  end function d2MyFunc
  
end program main
