module MODULE_CHPS_M1
  
contains
  
  !> initialize a mesh
  subroutine init_mesh(x,n,dx)
    implicit none
    integer :: n
    real(kind=8),dimension(0:N)::X
    real(kind=8)::dx
    integer :: i
    do i=0,n
       x(i) = i*dx
    end do
  end subroutine init_mesh
  !> print mesh
  subroutine print_mesh(x,n)
    implicit none
    integer :: n
    real(kind=8),dimension(0:N)::X
    integer :: i
    do i=0,n
       print*,i,x(i)
    end do
  end subroutine print_mesh
  !> print solution
  subroutine print_sol(x,T,n)
    implicit none
    integer :: n
    real(kind=8),dimension(0:N)::X,T
    integer :: i
    do i=0,n
       print*,i,x(i),t(i)
    end do
  end subroutine print_sol

  subroutine write_sol(filename,x,T,n)
    implicit none
    character(len=*) :: filename
    integer :: n
    real(kind=8),dimension(0:N)::X,T
    integer :: i
    open(unit=20,file=trim(filename))
    do i=0,n
       write(20,*),x(i),t(i)
    end do
    close(20)
  end subroutine write_sol
  
  subroutine set_variable(x,u,n,UserFunc)
    implicit none
    integer :: n
    real(kind=8),dimension(0:N)::X,u
    interface
       real(kind=8) function UserFunc(x)
         real(kind=8) :: x
       end function UserFunc
    end interface
    real(kind=8)::dx
    integer :: i
    do i=0,n
       u(i) = UserFunc(x(i))
    end do
  end subroutine set_variable
  
  real(kind=8) function compare(x,d2u,n,UserFunc)
    implicit none
    integer :: n
    real(kind=8),dimension(0:N)::X,d2u
    interface
       real(kind=8) function UserFunc(x)
         real(kind=8) :: x
       end function UserFunc
    end interface
    real(kind=8)::dx,err
    integer :: i
    
    dx = x(1)-x(0)
    err = 0
    do i=1,n-1
       err = max(err, abs( D2u(i) -  UserFunc(x(i ))))
    end do
    compare= err
  end function compare
  
  subroutine compute_dx2(x,U,D2U,N)
    implicit none
    integer :: n
    real(kind=8),dimension(0:N)::X,U,D2U
    real(kind=8) :: dx
    integer :: i
    dx = x(1)-x(0)
    do i=1,n-1
       D2U(I) = (U(I+1)-2*U(I) + U(I-1))/dx**2
    end do
  end subroutine compute_dx2

  
  subroutine compute_dx_centre(x,U,DU,N)
    implicit none
    integer :: n
    real(kind=8),dimension(0:N)::X,U,DU
    real(kind=8) :: dx
    integer :: i
    dx = x(1)-x(0)
    do i=1,n-1
       DU(I) = (U(I+1)- U(I-1))/(2*dx)
    end do
  end subroutine compute_dx_centre


  subroutine EulerExplicite_faire_n_pas_de_temps(x,dt,nu,U_new,U_old,N)
    implicit none
    integer :: n
    real(kind=8),dimension(0:N)::X,U_new,U_old
    real(kind=8) :: dx,dt,nu
    integer :: i
    dx = x(1)-x(0)
    do i=1,n-1
       U_NEW(I) = U_OLD(I) + NU*dt/dx**2*(U_OLD(I+1)-2*U_OLD(I) + U_OLD(I-1))
    end do
    U_NEW(0) = 0
    U_NEW(n) = 1
    
  end subroutine EulerExplicite_faire_n_pas_de_temps
  
  
end module MODULE_CHPS_M1
