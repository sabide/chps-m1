SOURCES_LOCALES_F90 = \
module_CHPS-M1.f90

FC=gfortran

OBJECTS+=$(SOURCES_LOCALES_F90:.f90=.o) $(SOURCES_LOCALES_F77:.f=.o) 

eval-derivatives: $(OBJECTS)
	$(FC) -Wunused $(FCFLAGS) $(INCS) -o $@.x $@.f90  $(OBJECTS) -lblas  -llapack 

Euler-Explicit: $(OBJECTS)
	$(FC) -Wunused $(FCFLAGS) $(INCS) -o $@.x $@.f90  $(OBJECTS) -lblas  -llapack 

%.o: %.f90
	$(FC) $(INCS)   $(FCFLAGS) -c $< -o $@
%.o: %.f
	$(FC)   $(INCS)   $(FCFLAGS) -c $< -o $@

clean:
	rm -f *.o *.x *.mod 

