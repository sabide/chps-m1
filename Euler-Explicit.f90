program main
  
  use MODULE_CHPS_M1
  implicit none
  integer,parameter :: n=10
  real(kind=8),dimension(0:N)::Un,Uo,x
  real(kind=8)::L,dx,tc,dt,cfl
  real(kind=8):: pi = acos(-1.)
  real(kind=8):: nu
  integer :: it
  
  L = 1
  dx = L/N
  nu = 1
  cfl = 0.49
  dt = CFL*dx**2/nu
  
  call init_mesh(x,n,dx)

  !> derivée seconde
  call set_variable(x,uo,n,zero)
  Uo(0) = 0
  Uo(n) = 1
  
  do it=1,10
     call EulerExplicite_faire_n_pas_de_temps(x,dt,nu,Un,Uo,N)
     Uo = Un
     print*,maxval(Un(0:n))
  end do
  call write_sol('EulerEplicit.dat',x,un,n)

contains
  
  real(kind=8) function zero(x)
    implicit none
    real(kind=8) :: x
    zero = 0d0
  end function Zero
  
  real(kind=8) function dMyFunc(x)
    implicit none
    real(kind=8) :: x
    DMyFunc = 2*x
  end function dMyFunc
  
  real(kind=8) function d2MyFunc(x)
    implicit none
    real(kind=8) :: x
    D2MyFunc = 2
  end function d2MyFunc
  
end program main
